#!/bin/bash
#
# Bash script that performs a backup of a Topten website
#
# $1 is the website identifier. Eg: topten-ch
# $2 is the MySQL database name for the CMS site.
# $3 is the MySQL database name for the Akeneo site
# $4 is the MongoDB database name for the product data of the site

mysql_root_user="root"
mysql_root_password="root"
backups_base_path="/home/vagonzal/backups"

do_db_backup() {
    local backup_path="$backups_base_path/$1"

    if [[ ! -d "$backup_path" ]]; then
        mkdir -p $backup_path
        echo "Created empty directory at $backup_path"
    fi

    # CMS backup
    mysqldump -u $mysql_root_user -p"$mysql_root_password" $2 > $backup_path/$2_`date +\%y\%m\%d`.sql
    tar -C $backup_path -cvJf $backup_path/$2_`date +\%y\%m\%d`.sql.tar.xz $2_`date +\%y\%m\%d`.sql
    echo "$2 MySQL database backed up succesfully!"

    # Akeneo backup
    mysqldump -u $mysql_root_user -p"$mysql_root_password" $3 > $backup_path/$3_`date +\%y\%m\%d`.sql
    tar -C $backup_path -cvJf $backup_path/$3_`date +\%y\%m\%d`.sql.tar.xz $3_`date +\%y\%m\%d`.sql
    echo "$3 MySQL database backed up succesfully!"

    # MongoDB backup
    mongodump --db $4 --out $backup_path/mongodb_`date +\%y\%m\%d`
    tar -cJf $backup_path/mongodb_`date +\%y\%m\%d`.tar.xz -C $backup_path/mongodb_`date +\%y\%m\%d` .
    echo "$4 MongoDB database backed up successfully!"

    # Remove original backup files
    rm -rf $backup_path/$2_`date +\%y\%m\%d`.sql $backup_path/$3_`date +\%y\%m\%d`.sql $backup_path/mongodb_`date +\%y\%m\%d`
    echo "Removed temporary files."

    # Remove old files
    find $backup_path -type f -name '*.tar.xz' -mtime +31 -exec rm {} \;
}

############################
## Add your settings here ##
############################

# examples
#do_db_backup topten-cl topten_cl akeneo_cl topten_cl
#do_db_backup topten-ch topten_ch akeneo_ch topten_ch