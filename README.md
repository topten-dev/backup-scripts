# Backup Scripts

## Usage

File backups for Topten websites
```bash
./files-backup.sh topten-id /path/to/topten /path/to/akeneo
```

Database backups for Topten websites
```bash
 ./db-backup.sh topten-id topten_database akeneo_database mongodb_database
```