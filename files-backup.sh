#!/bin/bash
#
# Backups CMS and Akeneo uploaded files
#
# $1 is the website identifier. Eg: topten-ch
# $2 is the Topten CMS root path
# $3 is the Akeneo root path

backups_base_path="/path/to/backup"

do_files_backup () {
    backup_path="$backups_base_path/$1"
    topten_files="$2/storage/web/source"
    akeneo_files="$3/app/uploads"

    if [[ ! -d $topten_files ]] && [[ ! -d $akeneo_files ]]; then
        echo "Paths not found!"
    fi

    tar -cJf $backup_path/topten_files_`date +\%y\%m\%d`.tar.xz -C $topten_files .
    tar -cJf $backup_path/akeneo_files_`date +\%y\%m\%d`.tar.xz -C $akeneo_files .
}

do_new_files_backup () {
    backup_path="$backups_base_path/$1"
    topten_files="$2/storage/web/source"
    akeneo_files="$3/var/file_storage"

    if [[ ! -d $topten_files ]] && [[ ! -d $akeneo_files ]]; then
        echo "Paths not found!"
    fi

    tar -cJf $backup_path/topten_files_`date +\%y\%m\%d`.tar.xz -C $topten_files .
    tar -cJf $backup_path/akeneo_files_`date +\%y\%m\%d`.tar.xz -C $akeneo_files .
}

############################
## Add your settings here ##
############################

# examples
#do_files_backup topten-cl /path/to/topten-cl /path/to/akeneo-cl
#do_files_backup topten-ch /path/to/topten-ch /path/to/akeneo-ch